Category.allow({
    remove: function(userId, doc) {
        var user = Meteor.users.findOne({ _id: userId })
        if (user && (user.roles && user.roles.indexOf(ROLES.Admin) > -1 || user.isPrimaryUser)) {
            return true;
        } else {
            return false
        }
    },
    insert: function(userId, doc) {
        var user = Meteor.users.findOne({ _id: userId })
        if (user && (user.roles && user.roles.indexOf(ROLES.Admin) > -1 || user.isPrimaryUser)) {
            return true;
        } else {
            return false
        }
    }    
});


Comments.allow({
    remove: function(userId, doc) {
        var user = Meteor.users.findOne({
            _id: userId
        })
        if (user && userId == doc.authorId) {
            return true;
        } else {
            return false
        }
    },
    insert: function(userId, doc) {
        var user = Meteor.users.findOne({
            _id: userId
        })
        if (user) {
            return true;
        } else {
            return false
        }
    },
    update: function(userId, doc, fieldNames, modifier) {
        return false;
    }
});

Post.allow({
    remove: function(userId, doc) {
        var user = Meteor.users.findOne({
            _id: userId
        })
        if (user && userId == doc.postedBy) {
            return true;
        } else {
            return false;
        }
    },
    insert: function(userId, doc) {
        var user = Meteor.users.findOne({
            _id: userId
        })
        if (user) {
            return true;
        } else {
            return false;
        }
    },
    update: function(userId, doc, fieldNames, modifier) {
        return false;
    }
});
