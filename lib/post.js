Post = new Mongo.Collection("post");
var Schemas = {};
Schemas.Post = new SimpleSchema({
    postedBy: {
        type: SimpleSchema.RegEx.Id,
    },
    title: {
        type: String,
        label: "Title",
    },    
    createdAt: {
        type: Date,
        defaultValue: new Date()
    },
    click_count: {
        type : Number,
        optional: true,
        defaultValue: 0
    },
    like_count: {
        type : Number,
        defaultValue: 0
    },
    comment_count: {
        type : Number,
        defaultValue: 0
    },
    updatedAt: {
        type: Date,
        autoValue: function() {
            if (this.isUpdate) {
                return new Date();
            }
        },
        denyInsert: true,
        optional: true
    },
    catType: {
        type: String,
        label: "Category"
    },    
    likeBy: {
        type: [SimpleSchema.RegEx.Id],
        optional: true
    },    
    flagBy: {
        type: [SimpleSchema.RegEx.Id],
        optional: true
    },   
    pictrue: {
        type: String
    },
    featured: {
        type: Boolean,
        optional: true
    },
    comments: {
        type: Array,
        optional: true
    },
    "comments.$": {
        type: String,
        optional: true
    }
});

Post.attachSchema(Schemas.Post);



Post.helpers({
    noOfLikes: function() {
        return this.likeBy && this.likeBy.length > 0 ? this.likeBy.length : 0; 
    },
    creator: function() {
        return Meteor.users.findOne(this.postedBy);
    }
});
