Comments = new Mongo.Collection("comments");

var Schemas = {};
Schemas.Comment = new SimpleSchema({
    creatorId: {
        type: SimpleSchema.RegEx.Id,
        autoValue: function() {
            return this.userId
        }
    },
    postId: {
        type: SimpleSchema.RegEx.Id,
    },
    comment: {
        type: String,
        autoform: {
            label: false,
            placeholder: "Enter your Comment"
        }
    },   
    commentedOn: {
        type: Date,
        autoValue: function() {
            if (this.isInsert) {
                return new Date();
            } else
                this.unset();
        },
        denyUpdate: true
    },
    likeBy: {
        type: [SimpleSchema.RegEx.Id],
        optional: true
    },
    replies: {
        type: [Object],
        optional: true
    },
    "replies.$.authorId": {
        type: SimpleSchema.RegEx.Id,
        autoValue : function() {
            return this.userId
        },        
    },
    "replies.$.reply": {
        type: String,
    },
    "replies.$.replyedOn": {
        type: Date,
        autoValue: function() {
            return new Date()
        }        
    },
    "replies.$._id": {
        type: SimpleSchema.RegEx.Id,
        autoValue: function() {
            if (!this.value)
                return Random.id();
            else
                return this.value;
        },        
    }    
});

Comments.attachSchema(Schemas.Comment);



Comments.helpers({
    noOfLikes: function() {
        return this.likeBy && this.likeBy.length > 0 ? this.likeBy.length : 0;
    },
    creator: function() {
        return Meteor.users.findOne(this.creatorId);
    },    
});
