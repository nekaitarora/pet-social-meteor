var deleteUser = new ReactiveVar(null);
var changeRole = new ReactiveVar(null);
var blockUser = new ReactiveVar(null);
var unblockUser = new ReactiveVar(null);
var filters = new ReactiveVar({});

Template.managePost.onRendered(function() {
    this.autorun(function() {
        Meteor.user() && (Roles.userIsInRole(Meteor.user()._id, ['admin']) || Meteor.user().isPrimaryUser) ? subscriptions.subscribe('post') : ''
    })
})

Template.managePost.helpers({
    posts: function() {
        
        return Post.find({});
    },
    settings: function() {
        return {
            rowsPerPage: 10,
            showFilter: true,
            fields: [{
                key: 'title',
                label: 'Title',
            }, {
                key: 'catType',
                label: 'Category',
                sortable: false,
                   
            }, {
                key: 'featured',
                label: 'isFeatured',
                sortable: true,
                fn: function(value, object) {
                    
                    if (value) {
                        return 'Yes'
                    } else {
                        return "No"
                    }
                }
            }, {
                key: 'createdAt',
                label: 'Created At',
                fn: function(value, object) {
                    var context = value;
                    if (context) {
                        return context.getFullYear() + '-' + ('00' + (context.getMonth() + 1)).substr(-2) + '-' + ('00' + context.getDate()).substr(-2) + ' ' + ('00' + context.getHours()).substr(-2) + ':' + ('00' + context.getMinutes()).substr(-2) + ':' + ('00' + context.getSeconds()).substr(-2);
                    }
                }
            }, {
                key: 'postedBy',
                label: 'Posted By',
                sortable: false,
                fn: function(value) {
                    return Meteor.users.findOne({_id:value}).username;
                }
            }, {
                key: 'Delete',
                label: 'Delete',
                sortable: false,
                fn: function(value, options) {
                    if (options && !options.isPrimaryUser)
                        return new Spacebars.SafeString('<button class="deletePost">Delete</button>');
                }
            }, {
                key: 'featured',
                label: 'featured/not featured',
                sortable: false,
                fn: function(value, options) {


                    if (value) {
                        return new Spacebars.SafeString('<button class="unfeatured">Remove from featured</button>');
                    } else {
                        return new Spacebars.SafeString('<button class="featured">make featured</button>');
                    }

                }
            }],
            showNavigation: 'auto'               
        }
    },
   
})


Template.managePost.helpers({
    roles: function() {
        return Roles.getAllRoles();
    }
})


Template.managePost.events({
    'click .reactive-table tbody tr': function(event) {      

        if (event.target.className == "featured") {
            Meteor.call('makePostfeatured', this._id, function(err) {
                if (err) {
                    err.reason ? console.log(err.reason) : console.log(err);
                }
            });
        }
        if (event.target.className == "unfeatured") {
            Meteor.call('makePostunfeatured', this._id, function(err) {
                if (err) {
                    err.reason ? console.log(err.reason) : console.log(err);
                }
            });
        }
        if (event.target.className == 'deletePost') {
            Meteor.call('deletePost', this._id, function(err) {
                if (err) {
                    err.reason ? console.log(err.reason) : console.log(err);
                }
            });
        }
    },
    
    
})
