var preview = new ReactiveVar(null);
var image = new ReactiveVar(null);
var loading = new ReactiveVar(false);

Template.uploadPost.helpers({
    settings: function() {
        return {
            position: "bottom",
            limit: 25,
            rules: [{
                collection: Category,
                // subscription: 'category',
                field: "name",
                noMatchTemplate: Template.noMatch,
                template: Template.imageSearch,                
                matchAll: true,                
                filter: {}, // needs to be set, otherwise sort is not working
                sort: true,
            }]
        };
    },
    preview: function() {
        return preview.get()
    },
    loading: function() {
        return loading.get();
    }
});

Template.uploadPost.events({
    'change #post_image_type': function(event) {
        FS.Utility.eachFile(event, function(file) {
            preview.set(file.name);
            var my_reader = new FileReader();            
            my_reader.onload = function(event) {
                var result = my_reader.result;
                // var buffer = new Uint8Array(result); // convert to binary
                document.getElementById("post_image").src = result;
                image.set(result)
            }
            my_reader.readAsDataURL(file);
        })
    },
    'submit #post-form': function(event) {
        event.preventDefault();
        Meteor.call("uploadImage", preview.get(), image.get(), function(err, result) {
            if (err) {
                console.log("Error", err);
                return
            } else {
                var json  = {};
                json.title = event.currentTarget[0].value;
                json.catType = event.currentTarget[1].value;
                json.featured = event.currentTarget[2].checked;
                json.postedBy = Meteor.userId();
                json.creatorName = Meteor.user().username;
                json.featuredOn = event.currentTarget[2].checked ? new Date() : undefined;
                json.pictrue = result;  
                loading.set(true);
                Post.insert(json,function(err,_id){
                    loading.set(false);
                    if(err) {
                        console.log('Error',err);
                        $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> ' + err + '.</div>')                            
                    } else {
                        $('#post-form').trigger("reset");
                        preview.set(null);
                        image.set(null);                        
                    }
                });
            }
        })
    }
})
