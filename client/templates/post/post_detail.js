var sharePost = new ReactiveVar(null); 
var commentLimit = new ReactiveVar(5);
var showReplyComment = new ReactiveVar(null);
AutoForm.hooks({
    addCommentForm: {
        before: {
            insert: function(doc) {
                doc.postId = Router.current().params.id
                return doc
            },            
        }
    }   
});

Template.post_detail.onRendered(function() {
    var self = this;
    this.autorun(function() {
        subscriptions.subscribe('comments', Router.current().params.id, commentLimit.get());
    })
    Meteor.call("addClickCount",Router.current().params.id, function(err, result) {
       if(err) {
        console.log("err",err);
       }
    })
})

Template.post_detail.helpers({
    'postDetail': function() {
        return Post.findOne({
            _id: Router.current().params.id
        })
    },
    postComments: function() {
        return Comments.find({
            postId: Router.current().params.id
        },{
            sort: {
                commentedOn: 1
            }
        })
    },
    'sharePost': function() {
       return sharePost.get() == this._id ? true : false;
    },
    absoluteURL : function() {
        return Meteor.absoluteUrl()
    },
    isLiked: function(likedBy) {
        return likedBy && likedBy.indexOf(Meteor.userId()) > -1 ? true : false;
    },   
    showMore: function() {
        var countObj =  CountComment.findOne({_id: Router.current().params.id})
        if(countObj && countObj.count) {
            console.log("CommentCount",countObj.count, commentLimit.get());
            return countObj.count > commentLimit.get();        
        } 
        return false 
    },
    showReply: function(id) {
        return  showReplyComment.get() == id
    },
    noOfComment: function() {
        // return Counts.get("noOfComment")
        // var countObj =  CountComment.findOne({_id: Router.current().params.id})
        var count =  Post.findOne({ _id: Router.current().params.id });
        return  count && count.comments && count.comments.length ? count.comments.length : 0;
    },
    replierObj: function(userId) {
        return Meteor.users.findOne({_id: userId})
    }
})

Template.post_detail.events({
    'click .likePost': function(event) {
        if (Meteor.userId()) {
            Meteor.call('likePost', this._id, function(err) {
                if (err) {
                    console.log(err.reason);
                }
            })
        } else {
            toastr.error("You have to sign in to like the post.");
        }
    },
    'click .sharePost': function(event) {
        if(sharePost.get()) {
          sharePost.set(undefined);
        } else {
            sharePost.set(this._id);
        }
    },
    'click .unlikePost': function(event) {
        if (Meteor.userId()) {
            Meteor.call('removelikePost', this._id, function(err) {
                if (err) {
                    console.log(err.message)
                }
            })
        } else {
            toastr.error("You have to sign in to remove like on this post.");
        }
    },
    'click .flagPost': function(event) {
        if (Meteor.userId()) {
            Meteor.call('flagPost', this._id, function(err) {
                if (err) {
                    console.log(err.reason);
                }
            })
        } else {
            toastr.error("You have to sign in to flag the post.");
        }
    },
    'click .unflagPost': function(event) {
        if (Meteor.userId()) {
            Meteor.call('unflagPost', this._id, function(err) {
                if (err) {
                    console.log(err.message)
                }
            })
        } else {
            toastr.error("You have to sign in to remove like on this post.");
        }
    },
    'click .commentLike': function(e) {
        e.preventDefault
        if (Meteor.userId()) {
            Meteor.call('likeComment', this._id, Meteor.userId())
        } else {
            toastr.error("You need to sign in to like this comment.");
        }
    },
    'click .removeCommentLike': function(e) {
        e.preventDefault
        if (Meteor.userId()) {
            Meteor.call('removeLikeComment', this._id, Meteor.userId())
        } else {
            toastr.error("You need to sign in to remove like on this comment.");
        }
    },
    'click .loadMore': function(e) {
        e.preventDefault();
        var limit = commentLimit.get();
        commentLimit.set(limit + 5);
    },
    "click .showReply": function(e) {
        e.preventDefault();
        showReplyComment.set(this._id);        
    },
    'submit #addReply': function(e) {
        e.preventDefault();
        if(!isEmpty(e.currentTarget[0].value)) {            
            Meteor.call('addReply', this._id, e.currentTarget[0].value, function(err) {
                if(err) {
                    console.log("err",err)
                    if(err.reason =="Author id is required")
                      toastr.error("You have to sign in to make reply on comment.")
                    else
                      console.log(err);
                } else {
                   $('#addReply').trigger("reset"); 
                }
            })
        }
    }
})
