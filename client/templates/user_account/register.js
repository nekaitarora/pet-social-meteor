var verifyEmail = new ReactiveVar(null);
var password = new ReactiveVar(null);
var confirmPassword = new ReactiveVar(null);

Template.register.helpers({
	'verifyEmail': function() {
		return verifyEmail.get();
	},
	'isFieldOk': function() {
       return password.get() == confirmPassword.get() ? false : true 
	}
})

Template.register.events({
    'submit #register-form': function(event) {
        event.preventDefault();
        var input = {};
        input.username = event.currentTarget[0].value;
        input.password = event.currentTarget[1].value;
        input.email = event.currentTarget[3].value;
        input.profile = {};
        input.profile.firstName = event.currentTarget[4].value;
        input.profile.lastName = event.currentTarget[5].value;
        Accounts.createUser(input, function(error) {
            console.log("Error", error);
            if(error) {
            	console.log(error.reason);
                error.reason ? $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> ' + error.reason + '.</div>') : ''                            
            } else {
            	verifyEmail.set(true);
            	Meteor.setTimeout(function(){
            		verifyEmail.set(false);
            		Router.go('login')		
            	},3000)
                $('#register-form').trigger("reset");
            }
        })
    },
   /* 'click .popup_sec .clos_btn' : function(event) {
    	event.preventDefault();
    	verifyEmail.set(true);
    },*/
    'input #confirmPassword': function(event) {
    	event.preventDefault();
    	confirmPassword.set(event.currentTarget.value)
    },
    'input #password': function(event) {
    	event.preventDefault();
    	password.set(event.currentTarget.value)
    }
})
