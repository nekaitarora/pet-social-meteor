var popup = new ReactiveVar(null); 


var doneCallback;

Accounts.onResetPasswordLink(function(token, done) {    
    Session.set('resetPasswordToken', token);
    doneCallback = done;
});

Template.forgotPassword.helpers({
   'popup': function() {
   	   return popup.get();
   } 
})
Template.forgotPassword.events({
	'submit #forgot-form': function(event) {
		 event.preventDefault();
		 var json = {};
		 json.email = event.currentTarget[0].value 
		 Accounts.forgotPassword(json,function(error){
		 	if(error) {
		 		error.reason ? $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> ' + error.reason + '.</div>') : ''                            
		 	} else {
		 		popup.set(true);	
		 		$('#forgot-form').trigger("reset");
		 	}	
		 });
	},
})