Template.popupSec.onRendered(function() {
	//bind a click event with pop up
    $('#clos_pop , .close_pop').click(function() {
        $('#pop_forgt').toggle("slide");
    });
})

Template.changePopup.onRendered(function() {
	//bind a click event with pop up
    $('#clos_pop , .close_pop').click(function() {
        $('#pop_forgt').toggle("slide");
        Template.changePassword.popup.set(false);
    });
})

Template.sharePopup.onRendered(function() {
	//bind a click event with pop up
    $('#clos_pop , .close_pop').click(function() {
        $('#pop_share').toggle("slide");
    });
})