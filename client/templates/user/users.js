var deleteUser = new ReactiveVar(null);
var changeRole = new ReactiveVar(null);
var blockUser = new ReactiveVar(null);
var unblockUser = new ReactiveVar(null);
var filters = new ReactiveVar({});

Template.users.onRendered(function() {
    this.autorun(function() {
        Meteor.user() && (Roles.userIsInRole(Meteor.user()._id, [ROLES.Admin]) || Meteor.user().isPrimaryUser) ? subscriptions.subscribe('usersAndRoles') : '';
    })
})

Template.users.helpers({
    users: function() {
        var obj = filters.get();
        var filter = {};
        if (obj && obj.role && obj.role != 'all') {
            filter.roles = {
                $in: [obj.role]
            }
        }
        if (obj && obj.status == 'active') {
            filter.isBlocked = false
        } else if (obj && obj.status == 'blocked') {
            filter.isBlocked = true
        }
        return Meteor.users.find(filter);
    },
    settings: function() {
        return {
            rowsPerPage: 10,
            showFilter: true,
            fields: [{
                key: 'username',
                label: 'User Name',
            }, {
                key: 'emails.0.address',
                label: 'Email',
                sortable: false
            }, {
                key: 'emails.0.verified',
                label: 'Verified',
                sortable: false
            }, {
                key: 'createdAt',
                label: 'Created At',
                fn: function(value, object) {
                    var context = value;
                    if (context) {
                        return context.getFullYear() + '-' + ('00' + (context.getMonth() + 1)).substr(-2) + '-' + ('00' + context.getDate()).substr(-2) + ' ' + ('00' + context.getHours()).substr(-2) + ':' + ('00' + context.getMinutes()).substr(-2) + ':' + ('00' + context.getSeconds()).substr(-2);
                    }
                }
            }, {
                key: 'profile.firstName',
                label: 'First Name',
                sortable: false
            }, {
                key: 'profile.lastName',
                label: 'Last Name',
                sortable: false
            }, {
                key: 'roles',
                label: 'Roles',
                sortable: false

            }, {
                key: 'Delete',
                label: 'Delete',
                sortable: false,
                fn: function(value, options) {
                    if (options && !options.isPrimaryUser)
                        return new Spacebars.SafeString('<button class="deleteUser">Delete</button>');
                }
            }, {
                key: 'change-role',
                label: 'Change Role',
                sortable: false,
                fn: function(value, options) {
                    if (options && !options.isPrimaryUser)
                        return new Spacebars.SafeString('<button class="changeRole">Change</button>');
                }
            }, {
                key: 'block/unblock',
                label: 'Block/Unblock',
                sortable: false,
                fn: function(value, options) {
                    if (options && !options.isPrimaryUser) {

                        if (options && options.isBlocked) {
                            return new Spacebars.SafeString('<button class="unblock">Unblock</button>');
                        } else {
                            return new Spacebars.SafeString('<button class="block">Block</button>');
                        }
                    }
                }
            }],
            showNavigation: 'auto'
        }
    },
    deleteUser: function() {
        return deleteUser.get();
    },
    changeRole: function() {
        return changeRole.get();
    },
    blockUser: function() {
        return blockUser.get();
    },
    unblockUser: function() {
        return unblockUser.get();
    },
    roles: function() {
        return Roles.getAllRoles();
    },
})


Template.change_role.helpers({
    roles: function() {
        return Roles.getAllRoles();
    },
    isEqual: function(name) {
        var user = Meteor.users.findOne({
            _id: changeRole.get()
        })
        if (user) {
            return user.roles.indexOf(name) > -1 ? true : false
        }
    },
    isOk: function(role) {
        return this.name == role;
    }
})


Template.users.events({
    'click .reactive-table tbody tr': function(event) {
        if (event.target.className == "deleteUser") {
            deleteUser.set(this._id);
        }
        if (event.target.className == "changeRole") {
            changeRole.set(this._id);
        }
        if (event.target.className == 'unblock') {
            unblockUser.set(this._id);
        }
        if (event.target.className == 'block') {
            blockUser.set(this._id);
        }
    },
    'click .close-modal': function(event) {
        event.preventDefault();
        deleteUser.set(null);
        changeRole.set(null);
        blockUser.set(null);
        unblockUser.set(null);
    },
    'click .confirmDelete': function(event) {
        event.preventDefault();
        Meteor.call('deleteUser', deleteUser.get(), function(err) {
            deleteUser.set(null)
            if (err) {
                err.reason ? alert(err.reason) : console.log(err);
            }
        });
    },
    'click .confirmChangeRole': function(event) {
        event.preventDefault();
        var permissions = $('input[name="changeRole"]:checked').map(function() {
            return this.value;
        }).get();
        Meteor.call('changeRole', changeRole.get(), permissions, function(err) {
            changeRole.set(null);
            if (err) {
                err.reason ? alert(err.reason) : console.log(err);
            }
        });
    },
    'click .blockUser': function(event) {
        event.preventDefault();
        Meteor.call('blockUnblockUser', blockUser.get(), true, function(err) {
            blockUser.set(null);
            if (err) {
                err.reason ? alert(err.reason) : console.log(err);
            }
        });
    },
    'click .unblockUser': function(event) {
        event.preventDefault();
        Meteor.call('blockUnblockUser', unblockUser.get(), false, function(err) {
            unblockUser.set(null);
            if (err) {
                err.reason ? alert(err.reason) : console.log(err);
            }
        });
    },
    'change #roleFilter': function(event) {
        var obj = filters.get()
        obj.role = event.currentTarget.value
        filters.set(obj);
    },
    'change #statusFilter': function(event) {
        var obj = filters.get()
        obj.status = event.currentTarget.value
        filters.set(obj);
    }
})