
Template.editAccount.helpers({
  user: function () {
  	return Meteor.user();
  },
  email: function() {
    var user = Meteor.user();
    return user && user.emails && user.emails[0] && user.emails[0].address;
  }

 })

Template.editAccount.events({
  'submit #edit-account-form': function (event) {
    event.preventDefault();
    var user= {
      "username": event.currentTarget[0].value,
      "emails.0.address": event.currentTarget[1].value,
      "profile.description": event.currentTarget[2].value,
      "profile.phone": event.currentTarget[3].value,
      "profile.address": event.currentTarget[4].value,
      "profile.experience": event.currentTarget[5].value
    };    
  	Meteor.call("changeProfile",user,function(err,result){
       if (err) {
         toastr.error(err.reason)
      } else {
         toastr.success('Changes saved')
      }
    })
  },  
})