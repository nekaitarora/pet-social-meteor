Template.account.onRendered(function() {
    this.autorun(function() {
      Session.set("changePassword",false); 
      Session.set("privacy",false); 
    })
})

Template.account.helpers({
    users: function() {
        var obj = filters.get();
        var filter = {};
        if (obj && obj.role && obj.role != 'all') {
            filter.roles = {
                $in: [obj.role]
            }
        }
        if (obj && obj.status == 'active') {
            filter.isBlocked = false
        } else if (obj && obj.status == 'blocked') {
            filter.isBlocked = true
        }
        return Meteor.users.find(filter);
    },
    cp: function() {
    	return Session.get("changePassword")
    },
    privacy: function() {
    	return Session.get("privacy");
    }
})




Template.account.events({
    'click .account': function(event) {
      for (i = 0; i <= $(".act_button li").length; i++) {            
		$($(".act_button li")[i]).removeClass("current")   
      } 
      $(".account").addClass("current");
       Session.set("changePassword",false); 
      Session.set("privacy",false);  
    },
    'click .cp': function(event) {
      for (i = 0; i <= $(".act_button li").length; i++) {            
		$($(".act_button li")[i]).removeClass("current")   
      } 
      $(".cp").addClass("current");
      Session.set("changePassword",true); 
      Session.set("privacy",false); 
    },
    'click .privacy': function(event) {
      for (i = 0; i <= $(".act_button li").length; i++) {            
		$($(".act_button li")[i]).removeClass("current")   
      } 
      $(".privacy").addClass("current");
      Session.set("changePassword",false); 
      Session.set("privacy",true);  
    }
})