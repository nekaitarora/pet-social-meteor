Template.layout.helpers({
    logedInUser: function() {
        return Meteor.user();
    }
})

Template.layout.events({
    'click .info_div1': function(event) {
        $('.open').toggle();
    },
    'click .logout': function(event){
        Meteor.logout();    
    }
})

Template.layout.onCreated(function() {
    var layout = this 
    layout.autorun(function(){        
        subscriptions.subscribe('category');           
        // subscriptions.subscribe('commentsCount',Session.get('categoryFilter'),Session.get("flagedPost"));
        layout.subscribe('post',Session.get('categoryFilter'),Session.get("headerFilter"),Session.get("flagedPost"));            
    })
    Blaze._allowJavascriptUrls();
    $(document.body).click(function(event) {
        if(!($(event.target).attr('class') == 'info_div1')) {
            $('.dropdown-menu').css('display','none');
        }
    });    
})
