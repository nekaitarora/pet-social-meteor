//Registration and Login validations

trimInput = function(value) {
    return value && value.replace(/^\s*|\s*$/g, '');
};

isNotEmpty = function(value) {
    if (value && value !== '') {
        return true;
    }
    $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> Please fill in all required fields.</div>')
    // console.log('Please fill in all required fields.');
    return false;
};
isNotEmptyArray = function(value,name) {
   if (value && value.length > 0) {
       return true;
   }
   $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> Please select atleast one '+name+' </div>')
   return false;
};
isSelected = function(value,label) {
   if (value && value !== '') {
        return true;
    }
    $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> Please select '+label+'.</div>')
    // console.log('Please select '+label);
    return false; 
}
isEmpty = function(value) {
    if (value && value !== '') {
        return false;
    }
    // console.log('Please fill in all required fields.');
    return true;
};

isNotEmptyArray = function(value,name) {
  if (value && value.length > 0) {
      return true;
  }
  $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> Please select atleast one '+name+' </div>')
  return false;
};
isNotLessThan5 = function(value) {
    if (value >= 5) {
        return true;
    }
    $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> The minimum amount accepted is $5.</div>')
    return false;
};

isEmail = function(value) {
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (filter.test(value)) {
        return true;
    }

    $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a>Please enter a valid email address.</div>')
    // console.log('Please enter a valid email address.');
    return false;
};

isEmailForLogin = function(value) {
    var filterEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (filterEmail.test(value)) {

        return true;

    } else {
        return false
    }

};

isalphaNumaric = function(value) {
    var filterUser = /^[A-Za-z0-9]{3,20}$/;

    if (filterUser.test(value)) {
        // console.log("i am called number is alphanumeric")
        return true
    } else {
        $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> Tin numer should be alpha-numeric.</div>');
        // console.log("Tin numer should be alpha-numeric")
        return false;
    }
};
isUserName = function(value) {
    var filterUser = /^[A-Za-z0-9_]{3,20}$/;

    if (filterUser.test(value)) {

        if (value[0] == "_") {
            $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> The username may only contain alphanumeric characters and underscores("_") and cannot begin with an underscore.</div>')
            // console.log("Please enter a valid username")
            return false;
        } else {
            return true;
        }

    } else {
        $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a>The username may only contain alphanumeric characters and underscores("_") and cannot begin with an underscore.</div>')
        // console.log("Please enter a valid username")
        return false
    }
};

isValidPassword = function(password) {

    var filter = /^[A-Za-z0-9!@#$%^&*()_]{6,20}$/;
    if (filter.test(password)) {
        if (password.length < 6) {
            $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> Your password should be 6 characters or longer.</div>')
            // console.log('Your password should be 6 characters or longer.');
            return false;
        } else {
            return true;
        }
    } else if (/\s/.test(password)) {
        $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> Your password should not contain whitespaces.</div>')
        // console.log('Your password should not contain whitespaces.');
        return false;
    }
};
isChecked = function(checked) {
    if ($('[name="userType"]').is(':checked')) {
        return true;
    } else {
        $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> Please indicate whether you are a Buyer or Seller.</div>')
        // console.log("Please check 1 type of user between seller/buyer")
        return false;
    }
};

areValidPasswords = function(password, confirm) {
    if (!isValidPassword(password)) {
        return false;
    }
    if (password !== confirm) {
        $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a>Your two passwords are not equivalent.</div>')
        // console.log('Your two passwords are not equivalent.');
        return false;
    }
    return true;
};

randomString = function() {
    //Creating Dynamic Random String           
    var text = "",
        possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}


passwordIncorrect = function() {
    $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> Incorrect Password .</div>')
}


/*
 * Credit Card Validation
 * Demo purposes, this will removed on 4 milestone
 *
 */

notEmptyPurchase = function(value) {
    var numberFilter = /^[0-9]+$/;
    if (numberFilter.test(value) && value >= 10 && value <= 9999) {
        return true;
    } else {
        $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> The credit range is from $10 to $9,999.</div>')
        return false;
    }
}
notEmptyCard = function(value) {
    var numberFilter = /^[0-9]+$/;
    if (numberFilter.test(value) && value.length === 16) {
        return true;
    } else {
        $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a>Please enter a valid credit card number.</div>')
        return false;
    }
}
notEmptyCardName = function(value) {
    var regexCardName = /^[a-zA-Z\s]*$/;
    if (regexCardName.test(value)) {
        return true;
    } else {
        $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a>Your name may not contain numbers or special characters.</div>')
        return false;
    }
}
notEmptyExpiry = function(value) {
    /*var numberFilter = /^[0-9]+$/ ;
      if (numberFilter.test(value) && value.length === 2){
          return true;
      }else{
       $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a>Please use the last two digits for Month and Year.</div>')
      return false;
    }*/
    if (value != 0) {
        return true;
    } else {
        $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a>Please select a month and year.</div>')
        return false;
    }
}
notEmptyCode = function(value) {
    var numberFilter = /^[0-9]+$/;
    if (numberFilter.test(value) && value.length === 3) {
        return true;
    } else {
        $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a>CVC is 3 digits long.</div>')
        return false;
    }
}
isNumber = function(value) {
    var numberFilter = /^[0-9]+$/;
    if (numberFilter.test(value)) {
        return true;
    } else {
        $(".alerta").show();
        $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> Only Numbers.</div>')
        return false;
    }
}
isNumberCheck = function(value) {
    var numberFilter = /^[0-9]+$/;
    if (numberFilter.test(value)) {
        return true;
    } else {
        return false;
    }
}

passwordSecure = function(value) {
    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
    if (re.test(value)) {
        return true;
    } else {
        $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> Please enter a password with, a minimum of 8 characters including at least one number, at least one upper case character, and at least one lower case character.</div>')
        return false;
    }
}