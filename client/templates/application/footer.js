Template.footer.helpers({
	notification : function() {
		return newPostUpdate();
	},
	notificationPopupShown: function() {
		return $('.showNewPosts').position() && $('.showNewPosts').position().top < $(window).scrollTop();
	}
})

Template.footer.events({
	'click .showNewPosts': function(event,t) {
		event.preventDefault();
		$(window).scrollTop(0);
		updatelastRefreshTime();
	}
})
